package com.badlogic.gdx;

import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Matrix4;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.*;


public class GameScreen extends ApplicationAdapter implements Screen, InputProcessor{
    final BakeryEscapery game;


    SpriteBatch batch;
    Sprite sprite, spriteML, spriteRL, spriteLL, spriteMU, spriteRU, spriteLU, spriteHand,
            spriteHand2,spriteHand3, spriteHand4, spriteHand5, spriteMunch, spriteMunch2,spriteShield, spriteCountM, spriteCountL,
            spritePipe;
    Texture donutImg, platform, sprinkle, marshmellow, jello, hand, munchkin, shieldZone, munchkinCounter, lives, thePipe;
    World world;
    Body body, bodyML, bodyRL, bodyLL, bodyMU, bodyRU, bodyLU, bodyHand,
            bodyHand2,bodyHand3, bodyHand4, bodyHand5, bodyMunch, bodyMunch2;
    Body bodyEdgeScreen;
    Box2DDebugRenderer debugRenderer;
    Matrix4 debugMatrix;
    OrthographicCamera camera;
    BitmapFont font;
    Music twoJump;
    Music gameOver;
    Music oneUp;
    Music loseLife;
    Music loop;
    int jump_counter = 0;
    boolean movingRight = false;
    boolean movingLeft = false;
    boolean holdingDown = false;
    boolean drawMunch = true;
    boolean drawMunch2 = true;
    boolean drawDonut = true;
    boolean spawnHand2 = false;
    boolean spawnHand3 = false;
    boolean spawnHand4 = false;
    boolean spawnHand5 = false;
    float ranNumHx = MathUtils.random(-3f, -2f);
    float ranNumHy = MathUtils.random(-2.5f, -2f);
    float ranNumH2x = MathUtils.random(3f, 2f);
    float ranNumH2y = MathUtils.random(3f, 2.5f);
    float ranNumH3x = MathUtils.random(-4f, -3f);
    float ranNumH3y = MathUtils.random(3f, 2f);
    float ranNumH4x = MathUtils.random(3f, 2f);
    float ranNumH4y = MathUtils.random(-4f, -3.5f);
    float ranNumH5x = MathUtils.random(4.5f, 3.5f);
    float ranNumH5y = MathUtils.random(-2f, -1.5f);
    int munchkinCount = 0;
    int extraLife = 0;
    int donutLives = 5;


    State state = State.RUN;

    final float PIXELS_TO_METERS = 100f;
    //these entities will be given to the platforms, walls, hands, and donut making it possible to collide
    final short PHYSICS_ENTITY = 0x1;    // 0001
    final short PHYSICS2_ENTITY = 0x1 <<2;    // 0100
    final short WORLD_ENTITY = 0x1 << 1; // 0010 or 0x2 in hex



    public GameScreen(final BakeryEscapery gam) {
        this.game = gam;


        batch = new SpriteBatch();
        donutImg = new Texture("donut.png");
        platform = new Texture("platform.png");
        sprinkle = new Texture("sprinkle.png");
        marshmellow = new Texture("marshmellow.png");
        jello = new Texture("jello.png");
        hand = new Texture(("hand.png"));
        munchkin = new Texture("munchkin.png");
        munchkinCounter = new Texture("munchkinCount.png");
        lives = new Texture("donutLives.png");
        shieldZone = new Texture("shieldZone.png");
        thePipe = new Texture("pipe.png");
        twoJump = Gdx.audio.newMusic(Gdx.files.internal("2jump.wav"));
        gameOver = Gdx.audio.newMusic(Gdx.files.internal("game_over.wav"));
        oneUp = Gdx.audio.newMusic(Gdx.files.internal("1up.wav"));
        loseLife = Gdx.audio.newMusic(Gdx.files.internal("loseLife.wav"));
        loop = Gdx.audio.newMusic(Gdx.files.internal("loop.mp3"));
        loop.setLooping(true);
        loop.play();
        loop.setVolume(0.2f);

        twoJump.setVolume(0.1f);
        camera = new OrthographicCamera();
        camera.setToOrtho(false, 900, 800);

        world = new World(new Vector2(0, -15f), true);

        spawnMuchnkin();
        spawnMunchkin2();
        spawnDonut();

        //placing the shield zones in the background as a visual representation of where the donut is safe
        spriteShield = new Sprite(shieldZone);
        spriteShield.setPosition(-spriteShield.getWidth() / 2, -spriteShield.getHeight() - 670 / 2);

        //placing the pipe in the background as a visual representation of where the hands will spawn
        spritePipe = new Sprite(thePipe);
        spritePipe.setPosition(-spritePipe.getWidth() / 2, -spritePipe.getHeight() + 800 / 2);

        //placing the munchkin sprite at top of screen for visual representation of munchkin count
        spriteCountM = new Sprite(munchkinCounter);
        spriteCountM.setPosition(-spriteCountM.getWidth() - 775 / 2, -spriteCountM.getHeight() + 790 / 2);

        //placing the munchkin sprite at top of screen for visual representation of the lives
        spriteCountL = new Sprite(lives);
        spriteCountL.setPosition(-spriteCountL.getWidth() - 610 / 2, -spriteCountL.getHeight() + 785 / 2);

        //defining the hand
        spriteHand = new Sprite(hand);
        spriteHand.setPosition(-spriteHand.getWidth() / 2, -spriteHand.getHeight() + 707 / 2);

        //creating the hand
        BodyDef bodyDefHand = new BodyDef();
        bodyDefHand.type = BodyDef.BodyType.KinematicBody;
        bodyDefHand.position.set((spriteHand.getX() + spriteHand.getWidth() / 2) /
                        PIXELS_TO_METERS,
                (spriteHand.getY() + spriteHand.getHeight() / 2) / PIXELS_TO_METERS);

        bodyHand = world.createBody(bodyDefHand);


        CircleShape shapeHand = new CircleShape();
        shapeHand.setRadius(spriteHand.getWidth() / 2.5f / PIXELS_TO_METERS);

        FixtureDef fixtureDefHand = new FixtureDef();
        fixtureDefHand.shape = shapeHand;
        fixtureDefHand.density = 0.5f;
        //fixtureDefHand.filter.categoryBits = PHYSICS2_ENTITY;

        bodyHand.createFixture(fixtureDefHand);
        shapeHand.dispose();

        //functions for spawning hands are below
        hand2Spawn();
        hand3Spawn();
        hand4Spawn();
        hand5Spawn();


        //START WORK ON PLATFORMS



        //define the middle lower platform, will be defined by the "ML" within variables
        spriteML = new Sprite(platform);
        spriteML.setPosition(-spriteML.getWidth() / 2, -spriteML.getHeight() - 475 / 2);
        BodyDef bodyDefMLPlatform = new BodyDef();
        PolygonShape shapeML = new PolygonShape();
        FixtureDef fixtureDefMLPlatform = new FixtureDef();
        spawnPlatform(spriteML, bodyDefMLPlatform, shapeML, fixtureDefMLPlatform);
        bodyML = world.createBody(bodyDefMLPlatform);
        bodyML.createFixture(fixtureDefMLPlatform);



        //define the right lower platform, will be defined by the "RL" within variables
        float ranNumRL = MathUtils.random(0f, 0.7f);
        if(ranNumRL <0.25f )
            spriteRL = new Sprite(sprinkle);
        else if(ranNumRL <0.55f)
            spriteRL = new Sprite(marshmellow);
        else
            spriteRL = new Sprite(jello);
        spriteRL.setPosition(-spriteRL.getWidth() + MathUtils.random(500, 765) / 2, -spriteRL.getHeight() - 210 / 2);
        BodyDef bodyDefRLPlatform = new BodyDef();
        PolygonShape shapeRL = new PolygonShape();
        FixtureDef fixtureDefRLPlatform = new FixtureDef();
        //fixtureDefRLPlatform.restitution = ranNumRL;
        spawnPlatform(spriteRL, bodyDefRLPlatform, shapeRL, fixtureDefRLPlatform);
        bodyRL = world.createBody(bodyDefRLPlatform);
        bodyRL.createFixture(fixtureDefRLPlatform);



        //define the left lower platform, will be defined by the "LL" within variables
        float ranNumLL = MathUtils.random(0f, 0.7f);
        if(ranNumLL <0.25f )
            spriteLL = new Sprite(sprinkle);
        else if(ranNumLL <0.55f)
            spriteLL = new Sprite(marshmellow);
        else
            spriteLL = new Sprite(jello);
        spriteLL.setPosition(-spriteLL.getWidth() + MathUtils.random(-465, -250) / 2, -spriteLL.getHeight() - 210 / 2);
        BodyDef bodyDefLLPlatform = new BodyDef();
        PolygonShape shapeLL = new PolygonShape();
        FixtureDef fixtureDefLLPlatform = new FixtureDef();
        //fixtureDefLLPlatform.restitution = ranNumLL;
        spawnPlatform(spriteLL, bodyDefLLPlatform, shapeLL, fixtureDefLLPlatform);
        bodyLL = world.createBody(bodyDefLLPlatform);
        bodyLL.createFixture(fixtureDefLLPlatform);



        //define the middle upper platform, will be defined by the "MU" within variables
        spriteMU = new Sprite(platform);
        spriteMU.setPosition(-spriteMU.getWidth() / 2, -spriteMU.getHeight() + 40 / 2);
        BodyDef bodyDefMUPlatform = new BodyDef();
        PolygonShape shapeMU = new PolygonShape();
        FixtureDef fixtureDefMUPlatform = new FixtureDef();
        spawnPlatform(spriteMU, bodyDefMUPlatform, shapeMU, fixtureDefMUPlatform);
        bodyMU = world.createBody(bodyDefMUPlatform);
        bodyMU.createFixture(fixtureDefMUPlatform);



        //define the right upper platform, will be defined by the "RU" within variables
        float ranNumRU = MathUtils.random(0f, 0.7f);
        if(ranNumRU <0.25f)
            spriteRU = new Sprite(sprinkle);
        else if(ranNumRU <0.55f)
            spriteRU = new Sprite(marshmellow);
        else
            spriteRU = new Sprite(jello);
        spriteRU.setPosition(-spriteRU.getWidth() + MathUtils.random(500, 765) / 2, -spriteRU.getHeight() + 275 / 2);
        BodyDef bodyDefRUPlatform = new BodyDef();
        PolygonShape shapeRU = new PolygonShape();
        FixtureDef fixtureDefRUPlatform = new FixtureDef();
        //fixtureDefRUPlatform.restitution = ranNumRU;
        spawnPlatform(spriteRU,bodyDefRUPlatform, shapeRU, fixtureDefRUPlatform);
        bodyRU = world.createBody(bodyDefRUPlatform);
        bodyRU.createFixture(fixtureDefRUPlatform);



        //define the left upper platform, will be defined by the "LU" within variables
        float ranNumLU = MathUtils.random(0f, 0.7f);
        if(ranNumLU <0.25f)
            spriteLU = new Sprite(sprinkle);
        else if(ranNumLU <0.55f)
            spriteLU = new Sprite(marshmellow);
        else
            spriteLU = new Sprite(jello);
        spriteLU.setPosition(-spriteLU.getWidth() + MathUtils.random(-465, -250) / 2, -spriteLU.getHeight() + 275 / 2);
        BodyDef bodyDefLUPlatform = new BodyDef();
        PolygonShape shapeLU = new PolygonShape();
        FixtureDef fixtureDefLUPlatform = new FixtureDef();
        //fixtureDefLUPlatform.restitution = ranNumLU;
        spawnPlatform(spriteLU, bodyDefLUPlatform, shapeLU, fixtureDefLUPlatform);
        bodyLU = world.createBody(bodyDefLUPlatform);
        bodyLU.createFixture(fixtureDefLUPlatform);



        //END WORK ON PLATFORMS

        //START WORK ON SCREEN EDGES


        //bodyDefBase and edgeShapeBase work together they are bottom border
        BodyDef bodyDefBase = new BodyDef();
        bodyDefBase.type = BodyDef.BodyType.StaticBody;
        float wB = Gdx.graphics.getWidth() / PIXELS_TO_METERS;
        float hB = Gdx.graphics.getHeight() / PIXELS_TO_METERS - 1 / PIXELS_TO_METERS;
        bodyDefBase.position.set(0, 0);
        FixtureDef fixtureDefBase = new FixtureDef();

        EdgeShape edgeShapeBase = new EdgeShape();
        edgeShapeBase.set(-wB / 2, -hB / 2, wB / 2, -hB / 2);
        fixtureDefBase.shape = edgeShapeBase;
        fixtureDefBase.friction = 100;
        fixtureDefBase.filter.categoryBits = PHYSICS2_ENTITY;
        fixtureDefBase.filter.categoryBits = WORLD_ENTITY;

        bodyEdgeScreen = world.createBody(bodyDefBase);
        bodyEdgeScreen.createFixture(fixtureDefBase);
        edgeShapeBase.dispose();


        //bodyDefRight and edgeShapeRight work together they are right border
        BodyDef bodyDefRight = new BodyDef();
        //1.5708 radians is exactly 90 degrees
        //angle is defined by radians
        bodyDefRight.angle = 1.5708f;
        bodyDefRight.type = BodyDef.BodyType.StaticBody;
        float wRi = Gdx.graphics.getWidth() / PIXELS_TO_METERS;
        float hRi = Gdx.graphics.getHeight() / PIXELS_TO_METERS + 100 / PIXELS_TO_METERS;
        bodyDefRight.position.set(0, 0);
        FixtureDef fixtureDefRight = new FixtureDef();

        EdgeShape edgeShapeRight = new EdgeShape();
        edgeShapeRight.set(-wRi / 2, -hRi / 2, wRi / 2, -hRi / 2);
        fixtureDefRight.shape = edgeShapeRight;
        fixtureDefRight.filter.categoryBits = PHYSICS2_ENTITY;
        fixtureDefRight.filter.categoryBits = WORLD_ENTITY;

        bodyEdgeScreen = world.createBody(bodyDefRight);
        bodyEdgeScreen.createFixture(fixtureDefRight);
        edgeShapeRight.dispose();


        //bodyDefLeft and edgeShapeLeft work together they are left border
        BodyDef bodyDefLeft = new BodyDef();
        //1.5708 radians is exactly 90 degrees
        //angle is defined by radians
        bodyDefLeft.angle = 1.5708f;
        bodyDefLeft.type = BodyDef.BodyType.StaticBody;
        float wL = Gdx.graphics.getWidth() / PIXELS_TO_METERS;
        // 1700 is defining where the border is in location to the screen?
        float hL = Gdx.graphics.getHeight() / PIXELS_TO_METERS - 1700 / PIXELS_TO_METERS;
        bodyDefLeft.position.set(0, 0);
        FixtureDef fixtureDefLeft = new FixtureDef();

        EdgeShape edgeShapeLeft = new EdgeShape();
        edgeShapeLeft.set(-wL / 2, -hL / 2, wL / 2, -hL / 2);
        fixtureDefLeft.shape = edgeShapeLeft;
        fixtureDefLeft.filter.categoryBits = PHYSICS2_ENTITY;
        fixtureDefLeft.filter.categoryBits = WORLD_ENTITY;

        bodyEdgeScreen = world.createBody(bodyDefLeft);
        bodyEdgeScreen.createFixture(fixtureDefLeft);
        edgeShapeLeft.dispose();

        //bodyDefRoof and edgeShapeRoof work together they are bottom border
        BodyDef bodyDefRoof = new BodyDef();
        bodyDefRoof.type = BodyDef.BodyType.StaticBody;
        float wR = Gdx.graphics.getWidth() / PIXELS_TO_METERS;
        //placed a little below the top of the screen so a score board and other info cant be overlapped by the donut
        float hR = Gdx.graphics.getHeight() / PIXELS_TO_METERS -1500 / PIXELS_TO_METERS;
        bodyDefRoof.position.set(0, 0);
        FixtureDef fixtureDefRoof = new FixtureDef();

        EdgeShape edgeShapeRoof = new EdgeShape();
        edgeShapeRoof.set(-wR / 2, -hR / 2, wR / 2, -hR / 2);
        fixtureDefRoof.shape = edgeShapeRoof;
        fixtureDefRoof.filter.categoryBits = PHYSICS2_ENTITY;
        fixtureDefRoof.filter.categoryBits = WORLD_ENTITY;

        bodyEdgeScreen = world.createBody(bodyDefRoof);
        bodyEdgeScreen.createFixture(fixtureDefRoof);
        edgeShapeRoof.dispose();

        //END WORK ON SCREEN EDGES



        Gdx.input.setInputProcessor(this);

        debugRenderer = new Box2DDebugRenderer();
        font = new BitmapFont();
        font.setColor(Color.WHITE);
        camera = new OrthographicCamera(Gdx.graphics.getWidth(), Gdx.graphics.
                getHeight());

        world.setContactListener(new ContactListener() {
                                     @Override
                                     public void beginContact(Contact contact) {
                                         Fixture fa = contact.getFixtureA();
                                         Fixture fb = contact.getFixtureB();

                                         //these contacts will decide what the donut hits and the proper result for such contact
                                         //this is a contact with a munchkin
                                         if(fa.getBody() == body && fb.getBody() == bodyMunch ||
                                                 fa.getBody() == bodyMunch && fb.getBody() == body){
                                             munchkinCount++;
                                             extraLife++;
                                             //this will respawn the munchkin
                                             drawMunch = false;
                                         }
                                         //this is a contact with a munchkin
                                         if(fa.getBody() == body && fb.getBody() == bodyMunch2 ||
                                                 fa.getBody() == bodyMunch2 && fb.getBody() == body){
                                             munchkinCount++;
                                             extraLife++;
                                             //this will respawn the munchkin
                                             drawMunch2 = false;
                                         }
                                         //this is a contact with a hand
                                         if(fa.getBody() == body && fb.getBody() == bodyHand ||
                                                 fa.getBody() == bodyHand && fb.getBody() == body){
                                             if(((body.getPosition().x > -0.5) && (body.getPosition().x < 0.5))
                                                     && (body.getPosition().y < -3) ){
                                             }
                                             else {
                                                 donutLives--;
                                                 loseLife.play();
                                                 extraLife = 0;
                                                 //this will cause the donut to be redrawn as if it is respawning
                                                 drawDonut = false;
                                             }
                                         }
                                         //this is a contact with a hand
                                         if(fa.getBody() == body && fb.getBody() == bodyHand2 ||
                                                 fa.getBody() == bodyHand2 && fb.getBody() == body){
                                             if(munchkinCount > 9) {
                                                 if (((body.getPosition().x > -0.5) && (body.getPosition().x < 0.5))
                                                         && (body.getPosition().y < -3) ){
                                                 } else {
                                                     donutLives--;
                                                     loseLife.play();
                                                     extraLife = 0;
                                                     //this will cause the donut to be redrawn as if it is respawning
                                                     drawDonut = false;
                                                 }
                                             }
                                         }
                                         //this is a contact with a hand
                                         if(fa.getBody() == body && fb.getBody() == bodyHand3 ||
                                                 fa.getBody() == bodyHand3 && fb.getBody() == body){
                                             if(munchkinCount >19) {
                                                 if (((body.getPosition().x > -0.5) && (body.getPosition().x < 0.5))
                                                         && (body.getPosition().y < -3) ){
                                                 } else {
                                                     donutLives--;
                                                     loseLife.play();
                                                     extraLife = 0;
                                                     //this will cause the donut to be redrawn as if it is respawning
                                                     drawDonut = false;
                                                 }
                                             }

                                         }
                                         //this is a contact with a hand
                                         if(fa.getBody() == body && fb.getBody() == bodyHand4 ||
                                                 fa.getBody() == bodyHand4 && fb.getBody() == body){
                                             if(munchkinCount >39) {
                                                 if (((body.getPosition().x > -0.5) && (body.getPosition().x < 0.5))
                                                         && (body.getPosition().y < -3) ){
                                                 } else {
                                                     donutLives--;
                                                     loseLife.play();
                                                     extraLife = 0;
                                                     //this will cause the donut to be redrawn as if it is respawning
                                                     drawDonut = false;
                                                 }
                                             }

                                         }
                                         //this is a contact with a hand
                                         if(fa.getBody() == body && fb.getBody() == bodyHand5 ||
                                                 fa.getBody() == bodyHand5 && fb.getBody() == body){
                                             if(munchkinCount >59) {
                                                 if (((body.getPosition().x > -0.5) && (body.getPosition().x < 0.5))
                                                         && (body.getPosition().y < -3) ){
                                                 } else {
                                                     donutLives--;
                                                     loseLife.play();
                                                     extraLife = 0;
                                                     //this will cause the donut to be redrawn as if it is respawning
                                                     drawDonut = false;
                                                 }
                                             }

                                         }

                                     }

                                     @Override
                                     public void endContact(Contact contact) {
                                         Fixture fa = contact.getFixtureA();
                                         Fixture fb = contact.getFixtureB();

                                         if(fa.getBody() == body && fb.getBody() == bodyMunch ||
                                                 fa.getBody() == bodyMunch && fb.getBody() == body){
                                             //this will respawn the munchkin somewhere else on the screen
                                             drawMunch = true;
                                         }
                                         if(fa.getBody() == body && fb.getBody() == bodyMunch2 ||
                                                 fa.getBody() == bodyMunch2 && fb.getBody() == body){
                                             //this will respawn the munchkin somewhere else on the screen
                                             drawMunch2 = true;
                                         }
                                         if(fa.getBody() == body && fb.getBody() == bodyHand ||
                                                 fa.getBody() == bodyHand && fb.getBody() == body){
                                             //this will cause the donut to be redrawn as if it is respawning
                                             drawDonut = true;
                                         }
                                         if(fa.getBody() == body && fb.getBody() == bodyHand2 ||
                                                 fa.getBody() == bodyHand2 && fb.getBody() == body){
                                             //this will cause the donut to be redrawn as if it is respawning
                                             drawDonut = true;
                                         }
                                         if(fa.getBody() == body && fb.getBody() == bodyHand3 ||
                                                 fa.getBody() == bodyHand3 && fb.getBody() == body){
                                             //this will cause the donut to be redrawn as if it is respawning
                                             drawDonut = true;
                                         }
                                         if(fa.getBody() == body && fb.getBody() == bodyHand4 ||
                                                 fa.getBody() == bodyHand4 && fb.getBody() == body){
                                             //this will cause the donut to be redrawn as if it is respawning
                                             drawDonut = true;
                                         }
                                         if(fa.getBody() == body && fb.getBody() == bodyHand5 ||
                                                 fa.getBody() == bodyHand5 && fb.getBody() == body){
                                             //this will cause the donut to be redrawn as if it is respawning
                                             drawDonut = true;
                                         }
                                     }

                                     @Override
                                     public void preSolve(Contact contact, Manifold oldManifold) {
                                     }

                                     @Override
                                     public void postSolve(Contact contact, ContactImpulse impulse) {
                                     }
                                 }
        );
    }

    //this method allows the hands to bounce off the walls and move in the opposite x direction
    public float moveHandX(Body unit, float x) {
        float x2 = x;
        if (unit.getPosition().x < -4.174 || unit.getPosition().x > 4.175) {
            x2 = x * -1;//MathUtils.random(-0.9f , -0.7f);
        }
        unit.setLinearVelocity(x2, unit.getLinearVelocity().y);
        return x2;
    }

    //this method allows the hands to bounce off the walls and move in the opposite y direction
    public float moveHandY(Body unit, float y){
        float y2 = y;
        if (unit.getPosition().y < -3.66 || unit.getPosition().y > 3.15)  {
            y2 = y * -1;
        }
        unit.setLinearVelocity(unit.getLinearVelocity().x, y2);
        return y2;
    }

    public void spawnMuchnkin(){
        spriteMunch = new Sprite(munchkin);
        spriteMunch.setPosition(-spriteMunch.getWidth() + MathUtils.random(-465, 765) / 2,
                -spriteMunch.getHeight() + MathUtils.random(-210, 700) / 2);

        //creating the munchkin
        BodyDef bodyDefMunch = new BodyDef();
        bodyDefMunch.type = BodyDef.BodyType.StaticBody;
        bodyDefMunch.position.set((spriteMunch.getX() + spriteMunch.getWidth() / 2) /
                        PIXELS_TO_METERS,
                (spriteMunch.getY() + spriteMunch.getHeight() / 2) / PIXELS_TO_METERS);

        bodyMunch = world.createBody(bodyDefMunch);

        final CircleShape shapeMunch = new CircleShape();
        shapeMunch.setRadius(spriteMunch.getWidth() / 3.5f / PIXELS_TO_METERS);

        final FixtureDef fixtureDefMunch = new FixtureDef();
        fixtureDefMunch.shape = shapeMunch;
        fixtureDefMunch.density = 0.1f;
        fixtureDefMunch.filter.categoryBits = PHYSICS_ENTITY;
        fixtureDefMunch.filter.maskBits = WORLD_ENTITY;

        bodyMunch.createFixture(fixtureDefMunch);
        shapeMunch.dispose();
    }

    public void spawnMunchkin2(){

        //defining second munchkin
        spriteMunch2 = new Sprite(munchkin);
        spriteMunch2.setPosition(-spriteMunch2.getWidth() + MathUtils.random(-465, 765) / 2,
                -spriteMunch2.getHeight() + MathUtils.random(-210, 700) / 2);

        //creating the munchkin
        BodyDef bodyDefMunch2 = new BodyDef();
        bodyDefMunch2.type = BodyDef.BodyType.StaticBody;
        bodyDefMunch2.position.set((spriteMunch2.getX() + spriteMunch2.getWidth() / 2) /
                        PIXELS_TO_METERS,
                (spriteMunch2.getY() + spriteMunch2.getHeight() / 2) / PIXELS_TO_METERS);

        bodyMunch2 = world.createBody(bodyDefMunch2);

        CircleShape shapeMunch2 = new CircleShape();
        shapeMunch2.setRadius(spriteMunch2.getWidth() / 3.5f / PIXELS_TO_METERS);

        FixtureDef fixtureDefMunch2 = new FixtureDef();
        fixtureDefMunch2.shape = shapeMunch2;
        fixtureDefMunch2.density = 0.1f;
        fixtureDefMunch2.filter.categoryBits = PHYSICS_ENTITY;
        fixtureDefMunch2.filter.maskBits = WORLD_ENTITY;

        bodyMunch2.createFixture(fixtureDefMunch2);
        shapeMunch2.dispose();

    }

    public void spawnDonut(){
        //define the donut, labelled as sprite because it is the most important sprite
        sprite = new Sprite(donutImg);
        sprite.setPosition(-sprite.getWidth() / 2, -sprite.getHeight() - 670 / 2);

        //creating the donut
        BodyDef bodyDef = new BodyDef();
        bodyDef.type = BodyDef.BodyType.DynamicBody;
        bodyDef.position.set((sprite.getX() + sprite.getWidth() / 2) /
                        PIXELS_TO_METERS,
                (sprite.getY() + sprite.getHeight() / 2) / PIXELS_TO_METERS);

        body = world.createBody(bodyDef);

        final CircleShape shape = new CircleShape();
        shape.setRadius(sprite.getWidth() / 2f / PIXELS_TO_METERS);

        final FixtureDef fixtureDefDonut = new FixtureDef();
        fixtureDefDonut.shape = shape;
        fixtureDefDonut.density = 0.1f;
        fixtureDefDonut.filter.categoryBits = PHYSICS_ENTITY;
        fixtureDefDonut.filter.categoryBits = WORLD_ENTITY;

        body.createFixture(fixtureDefDonut);
        shape.dispose();

    }

    public void hand2Spawn(){
        //defining the hand2
        spriteHand2 = new Sprite(hand);
        spriteHand2.setPosition(-spriteHand2.getWidth() / 2, -spriteHand2.getHeight() + 707 / 2);


        //creating the hand
        BodyDef bodyDefHand2 = new BodyDef();
        bodyDefHand2.type = BodyDef.BodyType.KinematicBody;
        bodyDefHand2.position.set((spriteHand2.getX() + spriteHand2.getWidth() / 2) /
                        PIXELS_TO_METERS,
                (spriteHand2.getY() + spriteHand2.getHeight() / 2) / PIXELS_TO_METERS);


        bodyHand2 = world.createBody(bodyDefHand2);
        CircleShape shapeHand2 = new CircleShape();
        shapeHand2.setRadius(spriteHand2.getWidth() / 2.5f / PIXELS_TO_METERS);
        FixtureDef fixtureDefHand2 = new FixtureDef();
        fixtureDefHand2.shape = shapeHand2;
        fixtureDefHand2.density = 0.5f;

        bodyHand2.createFixture(fixtureDefHand2);
        shapeHand2.dispose();
    }

    public void hand3Spawn(){
        //defining the hand3
        spriteHand3 = new Sprite(hand);
        spriteHand3.setPosition(-spriteHand3.getWidth() / 2, -spriteHand3.getHeight() + 707 / 2);

        //creating the hand
        BodyDef bodyDefHand3 = new BodyDef();
        bodyDefHand3.type = BodyDef.BodyType.KinematicBody;
        bodyDefHand3.position.set((spriteHand3.getX() + spriteHand3.getWidth() / 2) /
                        PIXELS_TO_METERS,
                (spriteHand3.getY() + spriteHand3.getHeight() / 2) / PIXELS_TO_METERS);

        bodyHand3 = world.createBody(bodyDefHand3);


        CircleShape shapeHand3 = new CircleShape();
        shapeHand3.setRadius(spriteHand3.getWidth() / 2.5f / PIXELS_TO_METERS);

        FixtureDef fixtureDefHand3 = new FixtureDef();
        fixtureDefHand3.shape = shapeHand3;
        fixtureDefHand3.density = 0.5f;

        bodyHand3.createFixture(fixtureDefHand3);
        shapeHand3.dispose();
    }

    public void hand4Spawn(){
        //defining the hand4
        spriteHand4 = new Sprite(hand);
        spriteHand4.setPosition(-spriteHand4.getWidth() / 2, -spriteHand4.getHeight() + 707 / 2);

        //creating the hand
        BodyDef bodyDefHand4 = new BodyDef();
        bodyDefHand4.type = BodyDef.BodyType.KinematicBody;
        bodyDefHand4.position.set((spriteHand4.getX() + spriteHand4.getWidth() / 2) /
                        PIXELS_TO_METERS,
                (spriteHand4.getY() + spriteHand4.getHeight() / 2) / PIXELS_TO_METERS);

        bodyHand4 = world.createBody(bodyDefHand4);


        CircleShape shapeHand4 = new CircleShape();
        shapeHand4.setRadius(spriteHand4.getWidth() / 2.5f / PIXELS_TO_METERS);

        FixtureDef fixtureDefHand4 = new FixtureDef();
        fixtureDefHand4.shape = shapeHand4;
        fixtureDefHand4.density = 0.5f;

        bodyHand4.createFixture(fixtureDefHand4);
        shapeHand4.dispose();
    }

    public void hand5Spawn(){
        //defining the hand5
        spriteHand5 = new Sprite(hand);
        spriteHand5.setPosition(-spriteHand5.getWidth() / 2, -spriteHand5.getHeight() + 707 / 2);

        //creating the hand
        BodyDef bodyDefHand5 = new BodyDef();
        bodyDefHand5.type = BodyDef.BodyType.KinematicBody;
        bodyDefHand5.position.set((spriteHand5.getX() + spriteHand5.getWidth() / 2) /
                        PIXELS_TO_METERS,
                (spriteHand5.getY() + spriteHand5.getHeight() / 2) / PIXELS_TO_METERS);

        bodyHand5 = world.createBody(bodyDefHand5);


        CircleShape shapeHand5 = new CircleShape();
        shapeHand5.setRadius(spriteHand5.getWidth() / 2.5f / PIXELS_TO_METERS);

        FixtureDef fixtureDefHand5 = new FixtureDef();
        fixtureDefHand5.shape = shapeHand5;
        fixtureDefHand5.density = 0.5f;

        bodyHand5.createFixture(fixtureDefHand5);
        shapeHand5.dispose();
    }

    //this method allows the code to be inserted and less cluttered above
    public void spawnPlatform(Sprite s, BodyDef bD,PolygonShape pS, FixtureDef fD){
        bD.type = BodyDef.BodyType.StaticBody;
        bD.position.set((s.getX() + s.getWidth() / 2) /
                        PIXELS_TO_METERS,
                (s.getY() + s.getHeight() / 2) / PIXELS_TO_METERS);

        pS.setAsBox(s.getWidth() / 2.3f / PIXELS_TO_METERS,
                s.getHeight() / 2.5f / PIXELS_TO_METERS);

        fD.shape = pS;
        fD.density = 10f;
        fD.friction = 100;
        fD.filter.categoryBits = PHYSICS_ENTITY;
        fD.filter.maskBits = WORLD_ENTITY;
        pS.dispose();
    }

    //these are all factors within the game that do not change during gameplay so they are called once
    public void spawnAll(){
        batch.draw(spriteShield, spriteShield.getX(), spriteShield.getY(), spriteShield.getOriginX(),
                spriteShield.getOriginY(),
                spriteShield.getWidth(), spriteShield.getHeight(), spriteShield.getScaleX(), spriteShield.
                        getScaleY(), spriteShield.getRotation());

        batch.draw(spritePipe, spritePipe.getX(), spritePipe.getY(), spritePipe.getOriginX(),
                spritePipe.getOriginY(),
                spritePipe.getWidth(), spritePipe.getHeight(), spritePipe.getScaleX(), spritePipe.
                        getScaleY(), spritePipe.getRotation());

        batch.draw(spriteCountM, spriteCountM.getX(), spriteCountM.getY(), spriteCountM.getOriginX(),
                spriteCountM.getOriginY(),
                spriteCountM.getWidth(), spriteCountM.getHeight(), spriteCountM.getScaleX(), spriteCountM.
                        getScaleY(), spriteCountM.getRotation());

        batch.draw(spriteCountL, spriteCountL.getX(), spriteCountL.getY(), spriteCountL.getOriginX(),
                spriteCountL.getOriginY(),
                spriteCountL.getWidth(), spriteCountL.getHeight(), spriteCountL.getScaleX(), spriteCountL.
                        getScaleY(), spriteCountL.getRotation());

        batch.draw(spriteML, spriteML.getX(), spriteML.getY(), spriteML.getOriginX(),
                spriteML.getOriginY(),
                spriteML.getWidth(), spriteML.getHeight(), spriteML.getScaleX(), spriteML.
                        getScaleY(), spriteML.getRotation());
        batch.draw(spriteRL, spriteRL.getX(), spriteRL.getY(), spriteRL.getOriginX(),
                spriteRL.getOriginY(),
                spriteRL.getWidth(), spriteRL.getHeight(), spriteRL.getScaleX(), spriteRL.
                        getScaleY(), spriteRL.getRotation());

        batch.draw(spriteLL, spriteLL.getX(), spriteLL.getY(), spriteLL.getOriginX(),
                spriteLL.getOriginY(),
                spriteLL.getWidth(), spriteLL.getHeight(), spriteLL.getScaleX(), spriteLL.
                        getScaleY(), spriteLL.getRotation());

        batch.draw(spriteMU, spriteMU.getX(), spriteMU.getY(), spriteMU.getOriginX(),
                spriteMU.getOriginY(),
                spriteMU.getWidth(), spriteMU.getHeight(), spriteMU.getScaleX(), spriteMU.
                        getScaleY(), spriteMU.getRotation());

        batch.draw(spriteRU, spriteRU.getX(), spriteRU.getY(), spriteRU.getOriginX(),
                spriteRU.getOriginY(),
                spriteRU.getWidth(), spriteRU.getHeight(), spriteRU.getScaleX(), spriteRU.
                        getScaleY(), spriteRU.getRotation());

        batch.draw(spriteLU, spriteLU.getX(), spriteLU.getY(), spriteLU.getOriginX(),
                spriteLU.getOriginY(),
                spriteLU.getWidth(), spriteLU.getHeight(), spriteLU.getScaleX(), spriteLU.
                        getScaleY(), spriteLU.getRotation());

        batch.draw(spriteHand, spriteHand.getX(), spriteHand.getY(), spriteHand.getOriginX(),
                spriteHand.getOriginY(),
                spriteHand.getWidth(), spriteHand.getHeight(), spriteHand.getScaleX(), spriteHand.
                        getScaleY(), spriteHand.getRotation());
    }



    @Override
    public void render(float delta) {

        camera.update();

        // tell the SpriteBatch to render in the
        // coordinate system specified by the camera.
        batch.setProjectionMatrix(camera.combined);


        Gdx.gl.glClearColor(.255f, .214f, .153f, 0.5f);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        batch.setProjectionMatrix(camera.combined);
        //debugMatrix = batch.getProjectionMatrix().cpy().scale(PIXELS_TO_METERS, PIXELS_TO_METERS, 0);

        //pause/resume
        if(donutLives<1){
        //makes it so you cant pause the game while in a game over phase
        }
        else {
            if (Gdx.input.isKeyJustPressed(Input.Keys.P)) {
                if (state == State.RUN) {
                    setGameState(State.PAUSE);
                } else {
                    setGameState(State.RUN);
                }
            }
        }

        batch.begin();

        if(extraLife== 10){
            donutLives= donutLives+1;
            oneUp.play();
            extraLife = 0;
        }

        spawnAll();

        if(munchkinCount>9)
            spawnHand2 = true;
        if(spawnHand2){
            batch.draw(spriteHand2, spriteHand2.getX(), spriteHand2.getY(), spriteHand2.getOriginX(),
                    spriteHand2.getOriginY(),
                    spriteHand2.getWidth(), spriteHand2.getHeight(), spriteHand2.getScaleX(), spriteHand2.
                            getScaleY(), spriteHand2.getRotation());
        }

        if(munchkinCount >19)
            spawnHand3 = true;
        if(spawnHand3) {
            batch.draw(spriteHand3, spriteHand3.getX(), spriteHand3.getY(), spriteHand3.getOriginX(),
                    spriteHand3.getOriginY(),
                    spriteHand3.getWidth(), spriteHand3.getHeight(), spriteHand3.getScaleX(), spriteHand3.
                            getScaleY(), spriteHand3.getRotation());
        }

        if(munchkinCount >39)
            spawnHand4 = true;
        if(spawnHand4) {
            batch.draw(spriteHand4, spriteHand4.getX(), spriteHand4.getY(), spriteHand4.getOriginX(),
                    spriteHand4.getOriginY(),
                    spriteHand4.getWidth(), spriteHand4.getHeight(), spriteHand4.getScaleX(), spriteHand4.
                            getScaleY(), spriteHand4.getRotation());
        }
        if(munchkinCount >59)
            spawnHand5 = true;
        if(spawnHand5) {
            batch.draw(spriteHand5, spriteHand5.getX(), spriteHand5.getY(), spriteHand5.getOriginX(),
                    spriteHand5.getOriginY(),
                    spriteHand5.getWidth(), spriteHand5.getHeight(), spriteHand5.getScaleX(), spriteHand5.
                            getScaleY(), spriteHand5.getRotation());
        }

        if(drawMunch) {
            batch.draw(spriteMunch, spriteMunch.getX(), spriteMunch.getY(), spriteMunch.getOriginX(),
                    spriteMunch.getOriginY(),
                    spriteMunch.getWidth(), spriteMunch.getHeight(), spriteMunch.getScaleX(), spriteMunch.
                            getScaleY(), spriteMunch.getRotation());
        }

        if(drawMunch2) {
            batch.draw(spriteMunch2, spriteMunch2.getX(), spriteMunch2.getY(), spriteMunch2.getOriginX(),
                    spriteMunch2.getOriginY(),
                    spriteMunch2.getWidth(), spriteMunch2.getHeight(), spriteMunch2.getScaleX(), spriteMunch2.
                            getScaleY(), spriteMunch2.getRotation());
        }

        if(drawDonut) {
            batch.draw(sprite, sprite.getX(), sprite.getY(), sprite.getOriginX(),
                    sprite.getOriginY(),
                    sprite.getWidth(), sprite.getHeight(), sprite.getScaleX(), sprite.
                            getScaleY(), sprite.getRotation());
        }


        //This can be changed to be a scoring feature. Currently we do not have the muchkins or a collision
        //detection with the hands so a scoring feature is pointless unlit we implement these features.
        //for now this is used to help us adjust the donut's movements/speeds
        font.draw(batch,
                "\n                 X  " + munchkinCount +
                        "              X " + donutLives,
                -Gdx.graphics.getWidth() / 2, Gdx.graphics.getHeight() / 2);
        batch.end();

        switch (state)
        {
            //if the state is RUN the game will continue to work without stopping
            case RUN:
                loop.play();
                // Step the physics simulation forward at a rate of 60hz
                world.step(1f / 60f, 6, 2);

                if(drawDonut) {
                    //donut sprite
                    sprite.setPosition((body.getPosition().x * PIXELS_TO_METERS) - sprite.
                                    getWidth() / 2,
                            (body.getPosition().y * PIXELS_TO_METERS) - sprite.getHeight() / 2);

                    sprite.setRotation((float) Math.toDegrees(body.getAngle()));
                }
                else{
                    world.destroyBody(body);
                    spawnDonut();
                }

                if(drawMunch) {
                    //munchkin sprite
                    spriteMunch.setPosition((bodyMunch.getPosition().x * PIXELS_TO_METERS) - spriteMunch.
                                    getWidth() / 2,
                            (bodyMunch.getPosition().y * PIXELS_TO_METERS) - spriteMunch.getHeight() / 2);
                }
                else {
                    world.destroyBody(bodyMunch);
                    spawnMuchnkin();
                }

                if(drawMunch2) {
                    //munchkin2 sprite
                    spriteMunch2.setPosition((bodyMunch2.getPosition().x * PIXELS_TO_METERS) - spriteMunch2.
                                    getWidth() / 2,
                            (bodyMunch2.getPosition().y * PIXELS_TO_METERS) - spriteMunch2.getHeight() / 2);
                }
                else {
                    world.destroyBody(bodyMunch2);
                    spawnMunchkin2();
                }

                //hand sprite
                spriteHand.setPosition((bodyHand.getPosition().x * PIXELS_TO_METERS) - spriteHand.
                                getWidth() / 2,
                        (bodyHand.getPosition().y * PIXELS_TO_METERS) - spriteHand.getHeight() / 2);
                moveHandX(bodyHand, ranNumHx);
                moveHandY(bodyHand, ranNumHy);
                ranNumHx = moveHandX(bodyHand, ranNumHx);
                ranNumHy = moveHandY(bodyHand, ranNumHy);

                if(spawnHand2) {
                    //hand2 sprite
                    spriteHand2.setPosition((bodyHand2.getPosition().x * PIXELS_TO_METERS) - spriteHand2.
                                    getWidth() / 2,
                            (bodyHand2.getPosition().y * PIXELS_TO_METERS) - spriteHand2.getHeight() / 2);
                    moveHandX(bodyHand2, ranNumH2x);
                    moveHandY(bodyHand2, ranNumH2y);
                    ranNumH2x = moveHandX(bodyHand2, ranNumH2x);
                    ranNumH2y = moveHandY(bodyHand2, ranNumH2y);
                }
                else{
                    world.destroyBody(bodyHand2);
                    hand2Spawn();
                }

                if(spawnHand3) {
                    //hand3 sprite
                    spriteHand3.setPosition((bodyHand3.getPosition().x * PIXELS_TO_METERS) - spriteHand3.
                                    getWidth() / 2,
                            (bodyHand3.getPosition().y * PIXELS_TO_METERS) - spriteHand3.getHeight() / 2);
                    moveHandX(bodyHand3, ranNumH3x);
                    moveHandY(bodyHand3, ranNumH3y);
                    ranNumH3x = moveHandX(bodyHand3, ranNumH3x);
                    ranNumH3y = moveHandY(bodyHand3, ranNumH3y);
                }
                else{
                    world.destroyBody(bodyHand3);
                    hand3Spawn();
                }

                if(spawnHand4) {
                    //hand4 sprite
                    spriteHand4.setPosition((bodyHand4.getPosition().x * PIXELS_TO_METERS) - spriteHand4.
                                    getWidth() / 2,
                            (bodyHand4.getPosition().y * PIXELS_TO_METERS) - spriteHand4.getHeight() / 2);
                    moveHandX(bodyHand4, ranNumH4x);
                    moveHandY(bodyHand4, ranNumH4y);
                    ranNumH4x = moveHandX(bodyHand4, ranNumH4x);
                    ranNumH4y = moveHandY(bodyHand4, ranNumH4y);
                }
                else{
                    world.destroyBody(bodyHand4);
                    hand4Spawn();
                }

                if(spawnHand5) {
                    //hand5 sprite
                    spriteHand5.setPosition((bodyHand5.getPosition().x * PIXELS_TO_METERS) - spriteHand5.
                                    getWidth() / 2,
                            (bodyHand5.getPosition().y * PIXELS_TO_METERS) - spriteHand5.getHeight() / 2);
                    moveHandX(bodyHand5, ranNumH5x);
                    moveHandY(bodyHand5, ranNumH5y);
                    ranNumH5x = moveHandX(bodyHand5, ranNumH5x);
                    ranNumH5y = moveHandY(bodyHand5, ranNumH5y);
                }
                else{
                    world.destroyBody(bodyHand5);
                    hand5Spawn();
                }

                //middle lower platform
                spriteML.setPosition((bodyML.getPosition().x * PIXELS_TO_METERS) - spriteML.
                                getWidth() / 2,
                        (bodyML.getPosition().y * PIXELS_TO_METERS) - spriteML.getHeight() / 2);

                //right lower platform
                spriteRL.setPosition((bodyRL.getPosition().x * PIXELS_TO_METERS) - spriteRL.
                                getWidth() / 2,
                        (bodyRL.getPosition().y * PIXELS_TO_METERS) - spriteLL.getHeight() / 2);


                //left lower platform
                spriteLL.setPosition((bodyLL.getPosition().x * PIXELS_TO_METERS) - spriteLL.
                                getWidth() / 2,
                        (bodyLL.getPosition().y * PIXELS_TO_METERS) - spriteLL.getHeight() / 2);


                // middle upper platform
                spriteMU.setPosition((bodyMU.getPosition().x * PIXELS_TO_METERS) - spriteMU.
                                getWidth() / 2,
                        (bodyMU.getPosition().y * PIXELS_TO_METERS) - spriteMU.getHeight() / 2);


                // right upper platform
                spriteRU.setPosition((bodyRU.getPosition().x * PIXELS_TO_METERS) - spriteRU.
                                getWidth() / 2,
                        (bodyRU.getPosition().y * PIXELS_TO_METERS) - spriteRU.getHeight() / 2);


                // left upper platform
                spriteLU.setPosition((bodyLU.getPosition().x * PIXELS_TO_METERS) - spriteLU.
                                getWidth() / 2,
                        (bodyLU.getPosition().y * PIXELS_TO_METERS) - spriteLU.getHeight() / 2);


                //set jump_counter to 0 if at rest vertically
                if (body.getLinearVelocity().y == 0)
                    jump_counter = 0;

                if (movingRight)
                    body.setLinearVelocity(2f, body.getLinearVelocity().y);

                if (movingLeft)
                    body.setLinearVelocity(-2f, body.getLinearVelocity().y);

                if (holdingDown)
                    body.setLinearVelocity(0f, body.getLinearVelocity().y);


                //debugRenderer.render(world, debugMatrix);
                if (donutLives < 1) {
                    setGameState(State.GAME_OVER);
                    loop.pause();
                    gameOver.play();
                }
                break;

            case PAUSE:
                //is state is PAUSE, the game will pause the game and display the paused screen info
                loop.pause();
                game.batch.begin();
                game.font.setColor(Color.WHITE);
                game.font.draw(game.batch, "PAUSED" +

                        "\n\nLEFT/RIGHT ARROWS move respectively" +
                        "\nUP ARROW or SPACE BAR to Jump" +
                        "\nHOLD DOWN ARROW to stop movement" +
                        "\n\nCollect the munchkins, don't get hit by the hands, lose all you lives and GAMEOVER!" +
                        "\nPress 'P' to unpause" +
                        "\nPress 'M' to return to main menu", 10, 750);
                game.font.setColor(Color.GREEN);
                game.font.draw(game.batch,
                        "10 Munchkins Collected: 2 Hands" +
                                "\n20 Munchkins Collected: 3 Hands" +
                                "\n40 Munchkins Collected: 4 Hands" +
                                "\n60 Munchkins Collected: 5 Hands", 500, 780);
                game.font.setColor(Color.PINK);
                game.font.draw(game.batch,
                        "Staying within the donut shield will protect you from losing lives." +
                                "\n                                                                                                            " +
                                "                   The hands can knock you out of the donut shield.", 5, 55);
                game.batch.end();
                if(Gdx.input.isKeyPressed(Input.Keys.M))
                {
                    game.setScreen((Screen) new MainMenuScreen(game));
                    dispose();
                }
                break;

            case GAME_OVER:
                game.batch.begin();
                game.font.setColor(Color.WHITE);
                game.font.draw(game.batch, "GAME OVER" +
                        "\nPress 'M' return to main menu", 20, 750);
                game.batch.end();
                if(Gdx.input.isKeyPressed(Input.Keys.M))
                {
                    game.setScreen((Screen) new MainMenuScreen(game));
                    dispose();
                }

        }
    }


    @Override
    public boolean keyDown(int keycode) {
        //this allows the player to complete a double jump without jumping too high
        if(keycode == Input.Keys.UP || keycode == Input.Keys.SPACE) {
            if(jump_counter == 2){

            }
            else if(jump_counter == 1){
                if(body.getLinearVelocity().y < 0){
                    body.applyForceToCenter(0f, 12f, true);
                    jump_counter++;
                    if(donutLives > 0) {
                        twoJump.play();
                    }

                }
                else {
                    body.applyForceToCenter(0f, 7f, true);
                    jump_counter++;
                    if(donutLives >0) {
                        twoJump.play();
                    }
                }
            }
            else {
                body.applyForceToCenter(0f, 10f, true);
                jump_counter++;

            }
        }
        //this will help the player stop the donut when it is on a platform or the ground
        if(keycode == Input.Keys.DOWN) {
            if(body.getLinearVelocity().y == 0) {
                holdingDown = true;
            }
            else
                holdingDown = false;
        }
        if(keycode == Input.Keys.RIGHT)
            movingRight = true;
        if(keycode == Input.Keys.LEFT)
            movingLeft = true;

        return true;
    }

    @Override
    public boolean keyUp(int keycode) {

        if(keycode == Input.Keys.RIGHT)
            movingRight = false;
        if(keycode == Input.Keys.LEFT)
            movingLeft = false;
        if(keycode == Input.Keys.DOWN)
            holdingDown = false;

        return true;
    }

    //Most of these are useless methods from here on, they are necessary to have to run

    @Override
    public boolean keyTyped(char character) {
        return false;
    }

    @Override
    public boolean touchDown(int screenX, int screenY, int pointer, int button) {
        return true;
    }

    @Override
    public boolean touchUp(int screenX, int screenY, int pointer, int button) {
        return false;
    }

    @Override
    public boolean touchDragged(int screenX, int screenY, int pointer) {
        return false;
    }

    @Override
    public boolean mouseMoved(int screenX, int screenY) {
        return false;
    }

    @Override
    public boolean scrolled(int amount) {
        return false;
    }


    @Override
    public void resize(int width, int height) {
    }

    @Override
    public void show() {
        // start the playback of the background music
        // when the screen is shown
        //Music.play();
    }

    @Override
    public void hide() {
    }

    @Override
    public void pause() {
        this.state = State.PAUSE;
    }

    @Override
    public void resume() {
        this.state = State.RUN;
    }

    public void setGameState(State s) {
        this.state = s;
    }

    @Override
    public void dispose() {
        donutImg.dispose();
        sprinkle.dispose();
        marshmellow.dispose();
        jello.dispose();
        world.dispose();
        hand.dispose();
        munchkin.dispose();
    }
}