package com.badlogic.gdx;



import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;

public class MainMenuScreen implements Screen {
    final BakeryEscapery game;
    OrthographicCamera camera;

    public MainMenuScreen(final BakeryEscapery gam) {
        game = gam;
        camera = new OrthographicCamera();
        camera.setToOrtho(false, 900, 800);
    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(0.1f, 0.1f, 0.2f, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        camera.update();
        game.batch.setProjectionMatrix(camera.combined);
        game.batch.begin();
        game.font.setColor(Color.WHITE);
        game.font.draw(game.batch, "BAKERY ESCAPERY!" +
                "\n\nDIRECTIONS:" +
                "\n\nLEFT/RIGHT ARROWS move respectively" +
                "\nUP ARROW or SPACE BAR to Jump" +
                "\nHOLD DOWN ARROW to stop movement" +
                "\nPress 'P' to pause/unpause and for directions" +
                "\n\nOBJECTIVE: " +
                "\n\nCollect the munchkins, don't get hit by the hands, lose all you lives and GAMEOVER!" +
                "\n\nHELP:" +
                "\n\nTime your jumps to perform a double jump effectively." +
                "\nStaying within the donut shield on the bottom of the screen will protect you from losing lives." +
                "\nThe hands can knock you out of the donut shield." +
                "\nTen consecutive munchkins collected without losing a life will reward you with an extra life." +
                "\n\nDANGER!" +
                "\n\nMore hands will spawn once:" +
                "\n10 Munchkins Collected: 2 Hands" +
                "\n20 Munchkins Collected: 3 Hands" +
                "\n40 Munchkins Collected: 4 Hands" +
                "\n60 Munchkins Collected: 5 Hands" +
                "\n\nPress 'ENTER' to begin!", 100, 750);
        game.batch.end();


        if (Gdx.input.isKeyPressed(Input.Keys.ENTER)) {
            game.setScreen((Screen) new GameScreen(game));
            dispose();
        }
    }

    @Override
    public void resize(int width, int height) {
    }

    @Override
    public void show() {
    }

    @Override
    public void hide() {
    }

    @Override
    public void pause() {
    }

    @Override
    public void resume() {
    }

    @Override
    public void dispose() {
    }
}